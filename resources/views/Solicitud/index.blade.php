@extends('theme.default')
@section('content')

<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Solicitudes </h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Solicitudes realizadas</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dtSolicitudes" width="100%" cellspacing="0">
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mdlSolicitud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">FORMATO PARA SOLICITUD DE MANTENIMIENTO CORRECTIVO/PREVENTIVO</h5>
      </div>
      <div class="modal-body">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Referencia al punto de las normas ISO 9001:2008 y 14001:2004</h6>
            </div>
            <div class="card-body">
              <input id="solicitud_id" name="solicitud_id" value="" type="hidden" />
  
                <div class="col-lg-12">
                  <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="INTERNO" name="solicitud_caracter">Servicio Interno
                    </label>
                  </div>
                  <div class="form-check-inline">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="EXTERNO" name="solicitud_caracter">Servicio Externo
                    </label>
                  </div>

                  <div class="form-group" style="float: right">
                    <input  id="solicitud_fecha" name="solicitud_fecha" type="date" class="form-control">
                  </div>
                </div>
                <div class="col-lg-12" style="margin-top: 15px;">
                  <div class="form-group">
                    <label for="solicitud_area_id">Área solicitante</label>
                    <select class="form-control" id="solicitud_area_id">
                      <option selected="" value="">Seleccionar</option>
                      @foreach($areas as $a)
                        <option value="{{ $a->area_id }}">{{ $a->area_clave }} -{{ $a->area_descripcion }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="form-group">
                    <label for="solicitud_solicitante">Nombre solicitante:</label>
                    <input type="text" class="form-control" name="solicitud_solicitante" id="solicitud_solicitante">
                  </div>
                </div>
                <div class="col-lg-4">
                  
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="solicitud_descripcion_servicio">Descripción del servicio</label>
                    <textarea class="form-control" rows="2" id="solicitud_descripcion_servicio" name="solicitud_descripcion_servicio"></textarea>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="solicitud_descripcion_problema">Mótivo solicitud</label>
                    <textarea class="form-control" rows="4" id="solicitud_descripcion_problema" name="solicitud_descripcion_problema"></textarea>
                  </div>
                </div>

            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="sendData()">Guardar solicitud</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
<script>
$(document).ready(function() {
    getRows();
});

var table = $('#dtSolicitudes').DataTable( {
        dom: 'Bfrtip',
        columns: [
          {"title": "Caracter",'data':'solicitud_caracter'},
          {"title": "Area",'data':'area.area_descripcion'},
          {"title": "Mótivo",'data':'solicitud_descripcion_problema'},
          {"title": "Solicitante",'data':'solicitud_solicitante'},
          {"title": "Fecha",'data':'solicitud_fechaOn'},
          {"title": "Estatus",'data':'solicitud_estatusOn'},
          {"title": "",'data':null, render: function ( data ) {
              return '<div class="btn-group"><button class="btn btn-sm btn-primary updateRow"><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-danger" onclick="deleteRow('+data.solicitud_id+')"><i class="fa fa-trash"></i></button></div>';
            }
          },
        ],
        buttons: [],
        initComplete: function () {
          $('.dt-buttons').html('<a class="btn btn-primary btn-icon-split" style="color:white!important;"  onclick="abrirModalAddSolicitud()"><span class="icon text-white-50"><i class="fas fa-plus"></i></span><span class="text"> Agregar solicitud</span></a>')
        }
    } );

function getRows(){

  $.ajax({ 
    method: "POST",
    url: "{{ route('lista_solicitud') }}",
    data:{'usuario':'*','estatus':'*', '_token': '{{csrf_token()}}'}
  })
  .done(function(data) {
    table.clear();
    if(data.success){
      table.rows.add(data.lista);
     
    }else{
      table.rows.add([]);
    }
     table.draw();
     
  });

}

function abrirModalAddSolicitud(){
  $('#mdlSolicitud').modal({
    backdrop: 'static',
    open:true
  })  
}

function getData(){
  return {
    '_token': '{{csrf_token()}}',
    'solicitud_id'            : $('#solicitud_id').val(),
    'solicitud_caracter'      : $('input[name=solicitud_caracter]:checked').val(),
    'solicitud_fecha'         : $('#solicitud_fecha').val(),
    'solicitud_area_id'       : $('#solicitud_area_id').val(),
    'solicitud_solicitante'   : $('#solicitud_solicitante').val(),
    'solicitud_descripcion_servicio'   : $('#solicitud_descripcion_servicio').val(),
    'solicitud_descripcion_problema'   : $('#solicitud_descripcion_problema').val()
  } 
}
function resetData(){
    $('#solicitud_id').val('');
    $('input[name=solicitud_caracter]:checked').prop('checked', false);
    $('#solicitud_fecha').val('');
    $('#solicitud_area_id').val('').change();
    $('#solicitud_solicitante').val('');
    $('#solicitud_descripcion_servicio').val('');
    $('#solicitud_descripcion_problema').val('');
}
function sendData(){
  let url = ($('#solicitud_id').val()>0)? "{{ route('actualizar_solicitud') }}" : "{{ route('agregar_solicitud') }}";
  $.ajax({ 
    method: "POST",
    url: url,
    data:  getData()
  })
  .done(function(data) {

    if(data.success){
      alert(data.msg);
      getRows();
      resetData();
      $('#mdlSolicitud').modal('hide');
    }else{
      alert(data.msg);
    }
  });
}

function deleteRow(row){
  if(row>0){

    $.ajax({ 
      method: "POST",
      url: "{{ route('eliminar_solicitud') }}",
      data:{'solicitud_id':row,'_token': '{{csrf_token()}}'}
    })
    .done(function(data) {
      if(data.success){
        getRows();
      }else{
        alert(data.msg);
      }
    });

  }else{
    alert('Debe seleccionar un registro');
  }
}

$('#dtSolicitudes tbody').on( 'click', '.updateRow', function () {
    var data = table.row( $(this).parents('tr') ).data();

    $('#solicitud_id').val(data.solicitud_id);
    $("input[name=solicitud_caracter][value="+data.solicitud_caracter+"]").prop("checked",true);
    $('#solicitud_fecha').val(data.solicitud_fecha);
    $('#solicitud_area_id').val(data.solicitud_area_id).change();
    $('#solicitud_solicitante').val(data.solicitud_solicitante);
    $('#solicitud_descripcion_servicio').val(data.solicitud_descripcion_servicio);
    $('#solicitud_descripcion_problema').val(data.solicitud_descripcion_problema);
    $('#mdlSolicitud').modal('show');

});

</script>
@endsection