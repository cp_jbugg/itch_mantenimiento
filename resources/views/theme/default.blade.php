<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Gestor Mantenimiento</title>
    <!-- Bootstrap Core CSS -->
    <link href="{!! asset('/theme/vendor/fontawesome-free/css/all.min.css') !!}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="{!! asset('/theme/css/sb-admin-2.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('/theme/vendor/datatables/dataTables.bootstrap4.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('/theme/vendor/datatables/buttons.bootstrap4.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('/theme/vendor/datepicker/gijgo.min.css') !!}" rel="stylesheet">
    <!-- MetisMenu CSS -->
</head>
<body id="page-top">
    <div id="wrapper">
        @include('theme.sidebar')
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                 @include('theme.header')
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <script src="/theme/vendor/jquery/jquery-3.3.1.js"></script>
    <script src="/theme/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/theme/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="/theme/vendor/datatables/dataTables.buttons.min.js"></script>
    <script src="/theme/vendor/datatables/buttons.bootstrap4.min.js"></script>

    <script src="/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/theme/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/theme/js/sb-admin-2.min.js"></script>
    <script src="/theme/vendor/datepicker/gijgo.min.js"></script>
    
    @yield('js')
</body>
</html>