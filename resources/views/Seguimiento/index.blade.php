@extends('theme.default')
@section('content')

<div class="container-fluid">
  <div class="col-lg-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Solicitudes Pendientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Respondidos</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dtSolicitudes" width="100%" cellspacing="0">
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="row" id="dtRespondidos">
              
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mdlSeguimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ORDEN DE TRABAJO DE MANTENIMIENTO</h5>
      </div>
      <div class="modal-body">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">MANTENIMIENTO <span id="textCaracter"></span> SOLICITADO EL <span id="textFecha"></span></h6>
            </div>
            <div class="card-body">
                <div class="col-lg-12">
                  <small>ÁREA SOLICITANTE</small> : <b id="textArea"></b>
                </div>
                <div class="col-lg-12">
                  <small>MÓTIVO</small> : <b id="textMotivo"></b>
                </div>
                <div class="col-lg-12">
                  <small>SOLICITANTE</small> : <b id="textSolicitante"></b>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <input id="solicitud_id" name="solicitud_id" value="" type="hidden" />
            <input id="seguimiento_rechazado" name="seguimiento_rechazado" value="" type="hidden" />
            <div class="card-body" id="fResponder">
              <div class="col-lg-12 row">
                <div class="col-lg-8">
                  <div class="form-group">
                    <label for="seguimiento_descripcion">Descripción trabajo</label>
                    <textarea class="form-control" rows="2" id="seguimiento_descripcion" name="seguimiento_descripcion"></textarea>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label for="seguimiento_fecha">Realizado</label>
                    <input  id="seguimiento_fecha" name="seguimiento_fecha" type="date" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="seguimiento_materiales">Materiales utilizados</label>
                  <textarea class="form-control" rows="4" id="seguimiento_materiales" name="seguimiento_materiales"></textarea>
                </div>
              </div>
              <div class="col-lg-12 row">
                <div class="col-lg-8">
                  <div class="form-group">
                    <label for="seguimiento_verificado">Verificado y liberado por:</label>
                    <input type="text" class="form-control" name="seguimiento_verificado" id="seguimiento_verificado">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label for="seguimiento_verificado_fecha">Fecha</label>
                    <input  id="seguimiento_verificado_fecha" name="seguimiento_verificado_fecha" type="date" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-lg-12 row">
                <div class="col-lg-8">
                  <div class="form-group">
                    <label for="seguimiento_aprobado">Aprobado por:</label>
                    <input type="text" class="form-control" name="seguimiento_aprobado" id="seguimiento_aprobado">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label for="seguimiento_aprobado_fecha">Fecha</label>
                    <input  id="seguimiento_aprobado_fecha" name="seguimiento_aprobado_fecha" type="date" class="form-control">
                  </div>
                </div>
              </div>
            </div>

            <div class="card-body" id="fRechazar">
              <div class="col-lg-12 row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="seguimiento_rechazado_motivo">Mótivo de rechazo</label>
                    <textarea class="form-control" rows="2" id="seguimiento_rechazado_motivo" name="seguimiento_rechazado_motivo"></textarea>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="sendData()">Guardar respuesta</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function() {   
    getPendientes();
    getRespondidos();
});
var table = $('#dtSolicitudes').DataTable( {
        dom: 'Bfrtip',
        columns: [
          {"title": "Caracter",'data':'solicitud_caracter'},
          {"title": "Area",'data':'area.area_descripcion'},
          {"title": "Mótivo",'data':'solicitud_descripcion_problema'},
          {"title": "Solicitante",'data':'solicitud_solicitante'},
          {"title": "Fecha",'data':'solicitud_fechaOn'},
          {"title": "Estatus",'data':'solicitud_estatusOn'},
          {"title": "",'data':null, render: function ( data ) {
              return '<div class="btn-group"><button class="btn btn-sm btn-primary updateRow" data-rechazado="0" title="Responder"><i class="fa fa-reply"></i></button><button class="btn btn-sm btn-danger updateRow" data-rechazado="1" title="Rechazar"><i class="fa fa-times"></i></button></div>';
            }
          },
        ],
        buttons: []
    } );

function getPendientes(){

  $.ajax({
    method: "POST",
    url: "{{ route('lista_solicitud') }}",
    data:{'usuario':'*','estatus':1, '_token': '{{csrf_token()}}'}
  })
  .done(function(data) {
    table.clear();
    if(data.success){
      table.rows.add(data.lista);
     
    }else{
      table.rows.add([]);
    }
     table.draw();
     
  });

}

function getRespondidos(){

  $.ajax({
    method: "POST",
    url: "{{ route('lista_solicitud') }}",
    data:{'usuario':'*','estatus':[2,3], '_token': '{{csrf_token()}}'}
  })
  .done(function(data) {

      $('#dtRespondidos').html('');
      $.each(data.lista, function( key, v ) {
          console.log(v);
        $html = '<div class="col-xl-4 col-md-4 mb-4">';
          $html += '<div class="card border-left-'+((v.solicitud_estatus==2)?'primary':'danger')+' shadow h-100 py-2">';
            $html += '<div class="card-body">';
              $html += '<div class="row no-gutters align-items-center">';
                $html += '<div class="col mr-2">';
                  $html += '<div class="text-xs font-weight-bold mb-1 text-gray-800"><b>SOLICITADO </b>'+v.solicitud_fechaOn+'<b> POR </b>'+v.solicitud_solicitante+'</div>';
                  $html += '<div class="text-xs font-weight-bold mb-1 text-gray-800"><b>AREA </b>'+v.area.area_descripcion+'</div>';
                  if(v.seguimiento.seguimiento_rechazado==1){
                  $html += '<div class="text-xs font-weight-bold mb-1 text-gray-800"><b>RESPUESTA </b><br>'+v.seguimiento.seguimiento_rechazado_motivo+'('+v.seguimiento.seguimiento_fechaOn+')</div>';
                  }else{
                    $html += '<div class="text-xs font-weight-bold mb-1 text-gray-800"><b>RESPUESTA </b><br>'+v.seguimiento.seguimiento_descripcion+' ('+v.seguimiento.seguimiento_fechaOn+')</div>';
                  }
                 //$html += '<div class="text-xs font-weight-bold text-gray-800"><b>ESTATUS</b> '+v.solicitud_estatusOn+'</div>';
                  $html += '<div class="text-xs font-weight-bold text-gray-800"><b>ESTATUS</b><span class="text-xl">'+v.solicitud_estatusOn+'</span></div>';
                $html += '</div>';
                $html += '<div class="col-auto">';
                  $html += '<i class="fas fa-'+((v.solicitud_estatus==2)?'check':'times')+' fa-2x text-gray-300"></i>';
                $html += '</div>';
              $html += '</div>';
            $html += '</div>';
          $html += '</div>';
        $html += '</div>';

            $('#dtRespondidos').append($html);
      });
 
    
      
    /*
    table.clear();
    if(data.success){
      table.rows.add(data.lista);
     
    }else{
      table.rows.add([]);
    }
     table.draw();*/
     
  });

}

function abrirModalAddSolicitud(){
  $('#mdlSeguimiento').modal({
    backdrop: 'static',
    open:true
  })  
}

function getData(){
  return {
    '_token': '{{csrf_token()}}',
    'solicitud_id'              : $('#solicitud_id').val(),
    'seguimiento_descripcion'   : $('#seguimiento_descripcion').val(),
    'seguimiento_fecha'         : $('#seguimiento_fecha').val(),
    'seguimiento_materiales'    : $('#seguimiento_materiales').val(),
    'seguimiento_verificado'    : $('#seguimiento_verificado').val(),
    'seguimiento_verificado_fecha'  : $('#seguimiento_verificado_fecha').val(),
    'seguimiento_aprobado'          : $('#seguimiento_aprobado').val(),
    'seguimiento_aprobado_fecha'    : $('#seguimiento_aprobado_fecha').val(),
    'seguimiento_rechazado'         : $('#seguimiento_rechazado').val(),
    'seguimiento_rechazado_motivo'  : $('#seguimiento_rechazado_motivo').val(),
  } 
}
function resetData(){
    $('#solicitud_id').val('');
    $('input[name=solicitud_caracter]:checked').prop('checked', false);
    $('#solicitud_fecha').val('');
    $('#solicitud_area_id').val(0).change();
    $('#solicitud_solicitante').val('');
    $('#solicitud_descripcion_servicio').val('');
    $('#solicitud_descripcion_problema').val('');

}
function sendData(){
  $.ajax({ 
    method: "POST",
    url: "{{ route('agregar_seguimiento') }}",
    data:  getData()
  })
  .done(function(data) {

    if(data.success){
      alert(data.msg);
      getPendientes();
      resetData();
      $('#mdlSeguimiento').modal('hide');
    }else{
      alert(data.msg);
    }
  });
}

$('#dtSolicitudes tbody').on( 'click', '.updateRow', function (e) {
    var data = table.row( $(this).parents('tr') ).data();
    
    $('#textCaracter').text(data.solicitud_caracter);
    $('#textFecha').text(data.solicitud_fechaOn);
    $('#textArea').text(data.area.area_descripcion);
    $('#textMotivo').text(data.solicitud_descripcion_problema);
    $('#textSolicitante').text(data.solicitud_solicitante);
    $('#solicitud_id').val(data.solicitud_id);
    $('#seguimiento_rechazado').val(e.currentTarget.dataset.rechazado);

    if(e.currentTarget.dataset.rechazado==1){
        $('#fResponder').hide()
        $('#fRechazar').show()
    }else{
        $('#fRechazar').hide()
        $('#fResponder').show()
    }
    $('#mdlSeguimiento').modal('show');

});

</script>
@endsection