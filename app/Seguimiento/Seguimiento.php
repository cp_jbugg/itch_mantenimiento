<?php

namespace App\Seguimiento;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Seguimiento extends Model
{
    protected $table = 'seguimientos';
    public $timestamps = false;

    /**
     * Primary Key
     *
     * @var string
     */
    protected $primaryKey  = 'seguimiento_id';

    protected $fillable = [
        'seguimiento_id',
		'solicitud_id',
		'seguimiento_descripcion',
		'seguimiento_materiales',
		'seguimiento_verificado',
		'seguimiento_aprobado',
		'seguimiento_fecha',
		'seguimiento_verificado_fecha',
		'seguimiento_aprobado_fecha',
		'seguimiento_estatus',
		'seguimiento_rechazado',
		'seguimiento_rechazado_motivo',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
    ];

    protected $appends = [
        'seguimiento_fechaOn',
        'seguimiento_estatusOn'
    ];

    public function getSeguimientoFechaOnAttribute(){

         $fecha = ' --/--/----';
        if(!is_null($this->seguimiento_fecha)){
            $st_fecha = Carbon::createFromFormat('Y-m-d', $this->seguimiento_fecha);
            $fecha = $st_fecha->format('d-m-Y');
        } 
        return $fecha;
    }

    public function getSeguimientoEstatusOnAttribute(){
        $estatus = "NO DEFINIDO";
        switch($this->seguimiento_estatus){
            case 1: $estatus = "NO ATENDIDO"; break;
            case 2: $estatus = "ATENDIDO"; break;
            case 3: $estatus = "FINALIZADO"; break;
        }
        return $estatus;
    }	
    /*
    public function area(){
        return $this->hasOne('App\Catalogo\Area', 'area_id', 'solicitud_area_id');
    }*/

}
