<?php

namespace App\Catalogo;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'cat_areas';
    public $timestamps = false;

    /**
     * Primary Key
     *
     * @var string
     */
    protected $primaryKey  = 'area_id';

}
