<?php

namespace App\Solicitud;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Solicitud extends Model
{
    protected $table = 'solicitudes';
    public $timestamps = false;

    /**
     * Primary Key
     *
     * @var string
     */
    protected $primaryKey  = 'solicitud_id';

    protected $fillable = [
        'solicitud_id',
        'solicitud_caracter',
        'solicitud_tipo',
        'solicitud_area_id',
        'solicitud_descripcion_servicio',
        'solicitud_descripcion_problema',
        'solicitud_observacion',
        'solicitud_asignado_id',
        'solicitud_solicitante_id',
        'solicitud_solicitante',
        'solicitud_fecha',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    protected $appends = [
        'solicitud_fechaOn',
        'solicitud_estatusOn'
    ];

    public function getSolicitudFechaOnAttribute(){

         $fecha = ' --/--/----';
        if(!is_null($this->solicitud_fecha)){
            $st_fecha = Carbon::createFromFormat('Y-m-d', $this->solicitud_fecha);
            $fecha = $st_fecha->format('d-m-Y');
        } 
        return $fecha;
    }

    public function getSolicitudEstatusOnAttribute(){
        $estatus = "NO DEFINIDO";
        switch($this->solicitud_estatus){
            case 1: $estatus = "NO ATENDIDO"; break;
            case 2: $estatus = "ATENDIDO"; break;
            case 3: $estatus = "RECHAZADO"; break;
        }
        return $estatus;
    }

    public function area(){
        return $this->hasOne('App\Catalogo\Area', 'area_id', 'solicitud_area_id');
    }
    public function seguimiento(){
        return $this->hasOne('App\Seguimiento\Seguimiento', 'solicitud_id', 'solicitud_id');
    }

}
