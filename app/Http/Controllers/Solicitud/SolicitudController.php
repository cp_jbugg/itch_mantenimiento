<?php

namespace App\Http\Controllers\Solicitud;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Solicitud\Solicitud;

class SolicitudController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Muestra el panel de solicitudes
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $user  = Auth::user();
        $areas = \DB::table('cat_areas')->get();

        return view('Solicitud.index')
                ->with('request', $request)
                ->with('areas', $areas)
                ->with('user', $user);
    }


    /**
     * Retorna un objeto con el listado de solicitudes
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listaSolicitud(Request $filters){   

        $query = Solicitud::where(function($q) use($filters){
                                if($filters->has('estatus')){
                                    $estatus = $filters->estatus;
                                    if($estatus!='*'){
                                        if(is_array($estatus)){
                                            $q->whereIn('solicitud_estatus',$estatus);
                                        }else{
                                            $q->where('solicitud_estatus',$estatus);
                                        }
                                    }
                                }
                                if($filters->has('usuario')){
                                    $estatus = $filters->usuario;

                                    /*if($estatus!='*'){
                                        if(is_array($estatus)){
                                            $q->whereIn('solicitud_estatus',$estatus);
                                        }else{
                                            $q->where('solicitud_estatus',$estatus);
                                        }
                                    }*/
                                }
                            })
                            ->with('area','seguimiento')
                            ;

                if($filters->has('search')){
                    if(strlen($filters->search)){
                        $q->where('solicitud_descripcion_problema', 'like', '%'.$filters->search.'%');
                       
                    }
                }

        $rows = $query->orderBy('solicitud_fecha','ASC')->get();
        $proceso = ($rows->count()>0)?true:false;

        return response()->json(array('success'=>$proceso,'lista'=>$rows,'msg'=>'Lista actualizada'));
    }

    public function agregarSolicitud(Request $request)
    {

        $proceso = false;
        $msg     =  "La Solicitud no pudo ser agregada.";

        DB::beginTransaction();
        try{
            $now = Carbon::now();

            $validator = Validator::make($request->all(), [
                'solicitud_caracter'                => 'required',
                'solicitud_area_id'                 => 'required',
                'solicitud_solicitante'             => 'required',
                'solicitud_descripcion_problema'    => 'required',
            ],[
                'solicitud_caracter.required' => 'Debe seleccionar el tipo de servicio.',
                'solicitud_area_id.required' => 'Debe seleccionar el area que solicita.',
                'solicitud_solicitante.required' => 'Debe indicar el solicitante.',
                'solicitud_descripcion_problema.required' => 'Es necesario él mótivo de la solicitud',
            ]);


            if(!$validator->fails()) {
                    
               $data =  [
                    'solicitud_caracter'    => $request->solicitud_caracter,
                    'solicitud_tipo'        => ($request->has('solicitud_tipo')? $request->solicitud_tipo: null),
                    'solicitud_area_id'     => $request->solicitud_area_id,
                    'solicitud_descripcion_servicio' => ($request->has('solicitud_descripcion_servicio')? $request->solicitud_descripcion_servicio: null),
                    'solicitud_descripcion_problema' => $request->solicitud_descripcion_problema,
                    'solicitud_observacion'         => null,
                    'solicitud_asignado_id'         => null,
                    'solicitud_solicitante_id'      => null,
                    'solicitud_solicitante'         => $request->solicitud_solicitante,
                    'solicitud_fecha'       => ($request->has('solicitud_fecha')? $request->solicitud_fecha:$now->format('Y-m-d') ),
                    'created_at'            => $now->format('Y-m-d H:i:s'),
                    'created_by'            => null,
                ];

                $solicitud = Solicitud::create($data);

                if($solicitud){
                    $proceso = true;
                    $msg     = 'La solicitud ha sido guardada con éxito';
                }

            }else{
                $msg = $validator->errors()->first();
            }

            if($proceso){
                DB::commit();
            }else{
                DB::rollback();
            }

        }catch(Exception $error){
            DB::rollback();
            $proceso = false;
            $msg = $error->getMessage();
        }
        return response()->json(array('success'=>$proceso,'msg'=>$msg));
    }
    public function actualizarSolicitud(Request $request){

        $proceso = false;
        $msg     =  "La Solicitud no pudo ser actualizada.";

        DB::beginTransaction();
        try{
            $now = Carbon::now();

            $validator = Validator::make($request->all(), [
                'solicitud_id'                => 'required',
                'solicitud_caracter'                => 'required',
                'solicitud_area_id'                 => 'required',
                'solicitud_solicitante'             => 'required',
                'solicitud_descripcion_problema'    => 'required',
            ],[
                'solicitud_id.required' => 'Debe seleccionar una solicitud por actualizar.',
                'solicitud_caracter.required' => 'Debe seleccionar el tipo de servicio.',
                'solicitud_area_id.required' => 'Debe seleccionar el area que solicita.',
                'solicitud_solicitante.required' => 'Debe indicar el solicitante.',
                'solicitud_descripcion_problema.required' => 'Es necesario él mótivo de la solicitud',
            ]);


            if(!$validator->fails()) {
               $data =  [
                    'solicitud_caracter'            => $request->solicitud_caracter,
                    'solicitud_tipo'                => ($request->has('solicitud_tipo')? $request->solicitud_tipo: null),
                    'solicitud_area_id'             => $request->solicitud_area_id,
                    'solicitud_descripcion_servicio' => ($request->has('solicitud_descripcion_servicio')? $request->solicitud_descripcion_servicio: null),
                    'solicitud_descripcion_problema' => $request->solicitud_descripcion_problema,
                    'solicitud_observacion'         => null,
                    'solicitud_asignado_id'         => null,
                    'solicitud_solicitante_id'      => null,
                    'solicitud_solicitante'         => $request->solicitud_solicitante,
                    'solicitud_fecha'       => ($request->has('solicitud_fecha')? $request->solicitud_fecha:$now->format('Y-m-d') ),
                    'updated_at'            => $now->format('Y-m-d H:i:s'),
                    'updated_by'            => null,
                ];

                $solicitud = Solicitud::find($request->solicitud_id);
            
                if($solicitud){
                    if($solicitud->update($data)){
                        $proceso = true;
                        $msg     = 'La solicitud ha sido actualizada con éxito';
                    }
                }

            }else{
                $msg = $validator->errors()->first();
            }

            if($proceso){
                DB::commit();
            }else{
                DB::rollback();
            }

        }catch(Exception $error){
            DB::rollback();
            $proceso = false;
            $msg = $error->getMessage();
        }
        return response()->json(array('success'=>$proceso,'msg'=>$msg));
    }


    public function eliminarSolicitud(Request $request){


        $proceso = false;
        $msg     =  "El registro no pudo ser eliminado.";

        DB::beginTransaction();
        try{

            $now = Carbon::now();
            if($request->has('solicitud_id')) {

                $solicitud = Solicitud::find($request->solicitud_id);

                if($solicitud){
                    $solicitud->delete();
                    $proceso = true;
                    $msg     = 'El registro ha sido eliminado con éxito';
                }
            }

            if($proceso){
                DB::commit();
            }else{
                DB::rollback();
            }

        }catch(Exception $error){
            DB::rollback();
            $proceso = false;
            $msg = $error->getMessage();
        }
        return response()->json(array('success'=>$proceso,'msg'=>$msg));

    }   
}
