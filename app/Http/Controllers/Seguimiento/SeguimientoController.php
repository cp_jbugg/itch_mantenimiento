<?php

namespace App\Http\Controllers\Seguimiento;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Solicitud\Solicitud;
use App\Seguimiento\Seguimiento;

class SeguimientoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Muestra el panel de solicitudes
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $user  = Auth::user();
        $areas = \DB::table('cat_areas')->get();

        return view('Seguimiento.index')
                ->with('request', $request)
                ->with('areas', $areas)
                ->with('user', $user);
    }

    public function agregarSeguimiento(Request $request)
    {

        $proceso = false;
        $msg     =  "El seguimiento no pudo ser guardado.";

        DB::beginTransaction();
        try{

            $now    = Carbon::now();
            $rules = [
                'seguimiento_descripcion'   => 'required',
                'seguimiento_verificado'    => 'required',
                'seguimiento_aprobado'      => 'required',
                'seguimiento_fecha'         => 'required',
            ];
            $msgs = [
                'seguimiento_descripcion.required'  => 'Es necesario la descripción del trabajo realizado.',
                'seguimiento_verificado.required'   => 'Debe indicar quien verifico.',
                'seguimiento_aprobado.required'     => 'Debe indicar quien aprobo.',
                'seguimiento_fecha.required'        => 'Debe indicar la fecha de realización.',
            ];

            if($request->seguimiento_rechazado==1){
                $rules  = ['seguimiento_rechazado_motivo' => 'required'];
                $msgs   = ['seguimiento_rechazado_motivo.required' => 'El mótivo del rechazo es obligatorio'];
            }

            $validator = Validator::make($request->all(),$rules,$msgs);


            if(!$validator->fails()) {
                    
               $data =  [
                    'solicitud_id'              => $request->solicitud_id,
                    'seguimiento_descripcion'   => $request->seguimiento_descripcion,
                    'seguimiento_materiales'    => $request->seguimiento_materiales,
                    'seguimiento_verificado'    => $request->seguimiento_verificado,
                    'seguimiento_aprobado'      => $request->seguimiento_aprobado,
                    'seguimiento_fecha'         => $request->seguimiento_fecha,
                    'seguimiento_verificado_fecha'  => $request->seguimiento_verificado_fecha,
                    'seguimiento_aprobado_fecha'    => $request->seguimiento_aprobado_fecha,
                    'seguimiento_rechazado'         => $request->seguimiento_rechazado,
                    'seguimiento_rechazado_motivo'  => $request->seguimiento_rechazado_motivo,
                    'seguimiento_estatus'           => 1,
                    'created_at'    => $now->format('Y-m-d H:i:s'),
                    'created_by'    => null,
                ];

                $seguimiento = Seguimiento::create($data);
                if($seguimiento){
                    $solicitud = Solicitud::find($seguimiento->solicitud_id);
                    if($solicitud){
                        $solicitud->solicitud_estatus = (($request->seguimiento_rechazado==1)?3:2);
                        $solicitud->save();
                        $proceso = true;
                        $msg     = 'La respuesta ha sido guardada con éxito';
                    }
                }

            }else{
                $msg = $validator->errors()->first();
            }

            if($proceso){
                DB::commit();
            }else{
                DB::rollback();
            }

        }catch(Exception $error){
            DB::rollback();
            $proceso = false;
            $msg = $error->getMessage();
        }
        return response()->json(array('success'=>$proceso,'msg'=>$msg));
    }

  /*  public function actualizarSolicitud(Request $request){

        $proceso = false;
        $msg     =  "La Solicitud no pudo ser actualizada.";

        DB::beginTransaction();
        try{
            $now = Carbon::now();

            $validator = Validator::make($request->all(), [
                'solicitud_id'                => 'required',
                'solicitud_caracter'                => 'required',
                'solicitud_area_id'                 => 'required',
                'solicitud_solicitante'             => 'required',
                'solicitud_descripcion_problema'    => 'required',
            ],[
                'solicitud_id.required' => 'Debe seleccionar una solicitud por actualizar.',
                'solicitud_caracter.required' => 'Debe seleccionar el tipo de servicio.',
                'solicitud_area_id.required' => 'Debe seleccionar el area que solicita.',
                'solicitud_solicitante.required' => 'Debe indicar el solicitante.',
                'solicitud_descripcion_problema.required' => 'Es necesario él mótivo de la solicitud',
            ]);


            if(!$validator->fails()) {
               $data =  [
                    'solicitud_caracter'            => $request->solicitud_caracter,
                    'solicitud_tipo'                => ($request->has('solicitud_tipo')? $request->solicitud_tipo: null),
                    'solicitud_area_id'             => $request->solicitud_area_id,
                    'solicitud_descripcion_servicio' => ($request->has('solicitud_descripcion_servicio')? $request->solicitud_descripcion_servicio: null),
                    'solicitud_descripcion_problema' => $request->solicitud_descripcion_problema,
                    'solicitud_observacion'         => null,
                    'solicitud_asignado_id'         => null,
                    'solicitud_solicitante_id'      => null,
                    'solicitud_solicitante'         => $request->solicitud_solicitante,
                    'solicitud_fecha'       => ($request->has('solicitud_fecha')? $request->solicitud_fecha:$now->format('Y-m-d') ),
                    'updated_at'            => $now->format('Y-m-d H:i:s'),
                    'updated_by'            => null,
                ];

                $solicitud = Solicitud::find($request->solicitud_id);
            
                if($solicitud){
                    if($solicitud->update($data)){
                        $proceso = true;
                        $msg     = 'La solicitud ha sido actualizada con éxito';
                    }
                }

            }else{
                $msg = $validator->errors()->first();
            }

            if($proceso){
                DB::commit();
            }else{
                DB::rollback();
            }

        }catch(Exception $error){
            DB::rollback();
            $proceso = false;
            $msg = $error->getMessage();
        }
        return response()->json(array('success'=>$proceso,'msg'=>$msg));
    }

*//*
    public function eliminarSolicitud(Request $request){


        $proceso = false;
        $msg     =  "El registro no pudo ser eliminado.";

        DB::beginTransaction();
        try{

            $now = Carbon::now();
            if($request->has('solicitud_id')) {

                $solicitud = Solicitud::find($request->solicitud_id);

                if($solicitud){
                    $solicitud->delete();
                    $proceso = true;
                    $msg     = 'El registro ha sido eliminado con éxito';
                }
            }

            if($proceso){
                DB::commit();
            }else{
                DB::rollback();
            }

        }catch(Exception $error){
            DB::rollback();
            $proceso = false;
            $msg = $error->getMessage();
        }
        return response()->json(array('success'=>$proceso,'msg'=>$msg));

    }*/   
}
