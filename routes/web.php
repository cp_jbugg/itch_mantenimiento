<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/solicitudes', 'Solicitud\SolicitudController@index')->name('principal_solicitudes');
Route::post('/lista/solicitud', 'Solicitud\SolicitudController@listaSolicitud')->name('lista_solicitud');
Route::post('/agregar/solicitud', 'Solicitud\SolicitudController@agregarSolicitud')->name('agregar_solicitud');
Route::post('/actualizar/solicitud', 'Solicitud\SolicitudController@actualizarSolicitud')->name('actualizar_solicitud');
Route::post('/eliminar/solicitud', 'Solicitud\SolicitudController@eliminarSolicitud')->name('eliminar_solicitud');


Route::prefix('seguimientos')->group(function(){
        Route::get('/','Seguimiento\SeguimientoController@index')->name('principal_seguimientos');
        Route::post('/agregar/seguimiento','Seguimiento\SeguimientoController@agregarSeguimiento')->name('agregar_seguimiento');
        
});